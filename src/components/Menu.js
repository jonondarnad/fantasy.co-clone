import React, { useState } from 'react';
import { HamburgerSlider } from 'react-animated-burgers';

const Menu = ({
    setSideBar,
    sideBar
              }) => {

    const [ isHovered, setIsHovered ] = useState(false);

    const handleHamburgerClick = () => {
        if (sideBar) {
            setSideBar(null);
        } else {
            setSideBar('sidebar');
        }
    }

    const handleMouseEnter = () => {
        if (!isHovered) {
            setIsHovered(true);
        }
    }

    const handleMouseLeave = () => {
        if (isHovered) {
            setIsHovered(false);
        }
    }

    return (
        <div className='menu'>
            {
                !sideBar && (
                    <>
                        <div
                            onClick={() => {
                                setSideBar('categories');
                            }}
                            className='menu-text'
                        >
                            All Categories
                        </div>
                        <div className='menu-separator'/>
                    </>
                )
            }

            <div
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
            >
                <HamburgerSlider
                    buttonWidth={30}
                    className={`hamburger-custom ${isHovered ? 'hover' : ''}`}
                    isActive={!!sideBar}
                    toggleButton={handleHamburgerClick}
                    barColor={isHovered ? '#fd0202' : sideBar ? '#000' : '#fff'}
                />
            </div>
        </div>
    );
};

export default Menu;
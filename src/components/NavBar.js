import React, { useState } from 'react';
import Menu from "./Menu";

const NavBar = ({
    sideBar,
    setSideBar
                }) => {
    return (

            <div className='nav-bar'>
                <img
                    alt={'Logo light'}
                    src={sideBar ? 'https://fantasy.co/assets/logoDefault.05b2a1cf.svg' : '/logoLight.svg'}
                    className='logo'
                />
                <Menu
                    setSideBar={setSideBar}
                    sideBar={sideBar}
                />
            </div>

    );
};

export default NavBar;
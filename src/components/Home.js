import React, {useState, useRef, useEffect} from 'react';
import NavBar from "./NavBar";
import First from "../pages/First";
import Second from "../pages/Second";
import Third from "../pages/Third";
import { SnapItem, SnapList, useScroll } from "react-snaplist-carousel";
import Sidebar from "react-sidebar";
import Logo from "./Logo";

const sections = [
    {
        text: 'What we do',
    },
    {
        text: 'Company',
    },
    {
        text: 'Contact',
    },
];

const links = [
    {
        path: 'https://fantasy.co/careers',
        text: 'Careers',
    },
    {
        path: 'https://www.linkedin.com/company/fantasy',
        text: 'LinkedIn',
    },
    {
        path: 'https://dribbble.com/Fantasy',
        text: 'Dribbble',
    },
    {
        path: 'https://www.instagram.com/designbyfantasy/',
        text: 'Instagram',
    },
    {
        path: 'https://twitter.com/F_i',
        text: 'Careers',
    },
    {
        path: 'https://fantasy.co/policy',
        text: 'Privacy Policy',
    },
];

const locations = [
    {
        path: 'https://goo.gl/maps/FJXg7rvcGxyzfVoRA',
        text: 'San Francisco',
    },
    {
        path: 'https://goo.gl/maps/HduwGVHBdnpbPmj29',
        text: 'New York',
    },
    {
        path: 'https://goo.gl/maps/nYDu78pcg4UicDUz8',
        text: 'London',
    },
];

const categories = [
    {
        text: 'Ecosystems',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Operating Systems',
        path: 'https://fantasy.co/what-we-do/operating-systems',
    },
    {
        text: 'Mobility',
        path: 'https://fantasy.co/what-we-do/mobility',
    },
    {
        text: 'Products',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Vision & Strategy',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'A.I. & Voice',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Healthcare',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'B2B Platforms',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'TV & Streaming Media',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Retail & Ecommerce',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Fashion & Luxury',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Websites',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
    {
        text: 'Travel & Hospitality',
        path: 'https://fantasy.co/what-we-do/ecosystems',
    },
];


const Home = () => {

    const [ sideBar, setSideBar ] = useState(null);

    const snapList = useRef(null);

    const goToElement = useScroll({ ref: snapList });

    useEffect(() => {
        // goToElement(2, { animationEnabled: false });
    }, []);

    const getSideBarContent = () => {
        if (!sideBar) {
            return null;
        }
        if (sideBar === 'categories') {
            return (
                <div className='sidebar-content'>
                    <div className='categories-wrapper'>
                        {
                            categories.map((category, index) => {
                                return (
                                    <div key={index} className={'category-link'}>
                                        <a target='_blank' href={category.path}>
                                            {category.text}
                                        </a>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            )
        }
        return (
            <div className='sidebar-content'>
                <div>
                    <Logo/>
                    <div className='sidebar-sections'>
                        {
                            sections.map(section => {
                                return (
                                    <div className='text-about' key={section.text}>
                                        {section.text}
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className='about-links-wrapper'>
                        {
                            links.map((link, index) => {
                                return (
                                    <div key={index} className={'about-link'}>
                                        <a target='_blank' href={link.path}>
                                            {link.text}
                                        </a>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                <div className='about-locations-wrapper'>
                    {
                        locations.map((location, index) => {
                            return (
                                <div key={index} className='about-location'>
                                    <a target='_blank' href={location.path}>
                                        {location.text}
                                    </a>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }


    const parentSections = [
        {
            component: <First/>,
        },
        {
            component: <Second/>,
        },
        {
            component: <Third/>,
        },
    ];

    return (
        <Sidebar
            pullRight={true}
            sidebar={getSideBarContent()}
            open={sideBar}
            onSetOpen={() => {
                setSideBar(null)
            }}
            overlayClassName='overlayClassName'
            styles={{
                overlay: {
                    opacity: 0.8,
                    backgroundColor: 'rgba(255, 255, 255, 0.6)',
                    transition: "opacity .4s ease-out, visibility .4s ease-out",
                },
                sidebar: {
                    transition: "transform .4s ease-out",
                },
                root: {
                    position: 'absolute',
                }
            }}
        >
            <div className='home'>
                <NavBar
                    sideBar={sideBar}
                    setSideBar={setSideBar}
                />
                <div>
                    <SnapList
                        ref={snapList}
                        direction={'vertical'}
                    >
                        {
                            parentSections.map((section, index) => (
                                <SnapItem key={index} snapAlign={'center'}>
                                    <div className='home-section'>
                                        {section.component}
                                    </div>
                                </SnapItem>
                            ))
                        }
                    </SnapList>
                </div>
            </div>
        </Sidebar>
    );
};

export default Home;
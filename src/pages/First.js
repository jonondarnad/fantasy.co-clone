import React from 'react';

const First = () => {
    return (
        <div style={{ display: "flex", flexDirection: 'column', justifyContent: 'flex-end', height: '100%', backgroundColor: '#000' }}>
           First page component
            <div className='footer'>
                <div className='footer-text'>Realizing the unimagined.</div>
                <div className='footer-text'>To make an impact.</div>
                <div className='footer-text'>For the world's biggest brands.</div>
                <button
                    className='footer-button'
                >
                    The work & verticals
                </button>
            </div>
        </div>
    );
};

export default First;